//
//  CountryTableViewCell.swift
//  Climaview
//
//  Created by Akshay Phadke on 11/12/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    static let cellIdentifier = "CountryTableViewCell"
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var isSelectedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        countryNameLabel.sizeToFit()
        self.contentView.layer.borderWidth = 0.35
        self.contentView.layer.borderColor = UIColor.white.cgColor
//        isSelectedImageView.layoutIfNeeded()
//        isSelectedImageView.layer.cornerRadius = isSelectedImageView.bounds.size.height / 2
    }

    func configure(with country: String) {
        countryNameLabel.text = country
        if let currentlySelectedCountry = UserDefaults.standard.value(forKey: UserDefaultsKey.isSelected.rawValue) as? String {
            if currentlySelectedCountry == country {
                isSelectedImageView.isHidden = false
            }
            else {
                isSelectedImageView.isHidden = true
            }
        }
        else {
            isSelectedImageView.isHidden = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
