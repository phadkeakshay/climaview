//
//  WeatherData.swift
//  Climaview
//
//  Created by Akshay Phadke on 13/12/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation

struct WeatherData: Encodable, Decodable {
    var rainfall: [Metric]
    var minimumTempratures: [Metric]
    var maximumTempratures: [Metric]
    
    init() {
        rainfall = [Metric]()
        minimumTempratures = [Metric]()
        maximumTempratures = [Metric]()
    }
    
    init(rainfall: [Metric], minimumTempratures: [Metric], maximumTempratures: [Metric]) {
        self.rainfall = rainfall
        self.minimumTempratures = minimumTempratures
        self.maximumTempratures = maximumTempratures
    }
    
}
