//
//  Country.swift
//  Climaview
//
//  Created by Akshay Phadke on 12/12/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation

class Country: Encodable, Decodable {
    let name: String
//    let rainfall: [Metric]
//    let minimumTempratures: [Metric]
//    let maximumTempratures: [Metric]
    var weatherData: WeatherData
    let dateComponents: [DateComponents]
    
//    init(name: String, rainfall: [Metric], minimumTempratures: [Metric], maximumtempratures: [Metric]) {
//        self.name = name
//        self.rainfall = rainfall
//        self.minimumTempratures = minimumTempratures
//        self.maximumTempratures = maximumtempratures
////        var rainfallDateComponentsSet = Set<DateComponents>()
////        var minimumTempraturesDateComponentsSet = Set<DateComponents>()
////        var maximumTempraturesDateComponentsSet = Set<DateComponents>()
////        for record in rainfall {
////            rainfallDateComponentsSet.insert(record.dateComponent)
////        }
////        for record in minimumTempratures {
////            minimumTempraturesDateComponentsSet.insert(record.dateComponent)
////        }
////        for record in maximumTempratures {
////            maximumTempraturesDateComponentsSet.insert(record.dateComponent)
////        }
////
////        let uniunSet1 = rainfallDateComponentsSet.union(minimumTempraturesDateComponentsSet)
////        let uniunSet2 = minimumTempraturesDateComponentsSet.union(maximumTempraturesDateComponentsSet)
////        let uniunFinalSet = uniunSet1.union(uniunSet2)
//        var _dateComponents = [DateComponents]()
//        for record in rainfall {
//            _dateComponents.append(record.dateComponent)
//        }
//        dateComponents = Array(_dateComponents)
//    }
    
    init(name: String, weatherData: WeatherData) {
        self.name = name
        self.weatherData = weatherData
//        self.rainfall = rainfall
//        self.minimumTempratures = minimumTempratures
//        self.maximumTempratures = maximumtempratures
        //        var rainfallDateComponentsSet = Set<DateComponents>()
        //        var minimumTempraturesDateComponentsSet = Set<DateComponents>()
        //        var maximumTempraturesDateComponentsSet = Set<DateComponents>()
        //        for record in rainfall {
        //            rainfallDateComponentsSet.insert(record.dateComponent)
        //        }
        //        for record in minimumTempratures {
        //            minimumTempraturesDateComponentsSet.insert(record.dateComponent)
        //        }
        //        for record in maximumTempratures {
        //            maximumTempraturesDateComponentsSet.insert(record.dateComponent)
        //        }
        //
        //        let uniunSet1 = rainfallDateComponentsSet.union(minimumTempraturesDateComponentsSet)
        //        let uniunSet2 = minimumTempraturesDateComponentsSet.union(maximumTempraturesDateComponentsSet)
        //        let uniunFinalSet = uniunSet1.union(uniunSet2)
        var _dateComponents = [DateComponents]()
        for record in weatherData.rainfall {
            _dateComponents.append(record.dateComponent)
        }
        dateComponents = Array(_dateComponents)
    }
    
}
