//
//  Metric.swift
//  Climaview
//
//  Created by Akshay Phadke on 12/12/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation

struct Metric: Encodable, Decodable {
    
    enum MetricType: String, Encodable, Decodable {
        case rainfall = "Rainfall",
        minimumTemprature = "Minimum Temprature",
        maximumTemprature = "Maximum Temprature"
    }
    
    enum MetricParameterName: String, Encodable, Decodable {
        case rainfall = "Rainfall",
        minimumTemprature = "Tmin",
        maximumTemprature = "Tmax"
    }
    
    enum Key: String, Encodable, Decodable {
        case value, year, month
    }
    
    let ID: Int
    let value: Double
    let metricType: MetricType
    let year: Int
    let month: Int
    let dateComponent: DateComponents
    
    
    init(json: [String: Any], metricType: MetricType, ID: Int) {
        value = json[Key.value.rawValue] as? Double ?? 0.0
        year = json[Key.year.rawValue] as? Int ?? 0
        month = json[Key.month.rawValue] as? Int ?? 0
        self.metricType = metricType
        self.ID = ID
        var _dateComponent = DateComponents()
        _dateComponent.year = year
        _dateComponent.month = month
        dateComponent = _dateComponent
    }
}
