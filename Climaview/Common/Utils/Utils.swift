//
//  Utils.swift
//  Climaview
//
//  Created by Akshay Phadke on 11/12/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class Utils {
    
    static func presentAlertController(withTitle title: String?, withMessage message: String, forViewController viewController : UIViewController? = nil, okCompletionHandler : @escaping (() -> ())) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
        //            // ...
        //        }
        //        alertController.addAction(cancelAction)
        let actionOk = UIAlertAction(title: "Ok", style: .default) { (action) in
            okCompletionHandler()
        }
        alertController.addAction(actionOk)
        DispatchQueue.main.async {
            if let viewControllerValue = viewController {
                viewControllerValue.present(alertController, animated: true, completion: nil)
            }
            else {
                guard let navigationController = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as? UINavigationController else { return }
                if let _ = navigationController.topViewController?.presentedViewController {
                    
                }
                else {
                    navigationController.topViewController?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    static func monthName(for month: Int) -> String {
        switch month {
            case 1:
                return "January"
            case 2:
                return "February"
            case 3:
                return "March"
            case 4:
                return "April"
            case 5:
                return "May"
            case 6:
                return "June"
            case 7:
                return "July"
            case 8:
                return "August"
            case 9:
                return "September"
            case 10:
                return "October"
            case 11:
                return "November"
            case 12:
                return "December"
            default:
                return ""
        }
    }
    
    static func monthIndex(for monthName: String) -> Int {
        switch monthName {
            case "January":
                return 1
            case "February":
                return 2
            case "March":
                return 3
            case "April":
                return 4
            case "May":
                return 5
            case "June":
                return 6
            case "July":
                return 7
            case "August":
                return 8
            case "September":
                return 9
            case "October":
                return 10
            case "November":
                return 11
            case "December":
                return 12
            default:
                return 0
        }
    }
    
    static func showProgressHUD(for view: UIView) {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: view, animated: true)
        }
    }
    
    static func hideProgressHUD(for view: UIView) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
}

extension DateComponents {
    enum DateComponent {
        case month, year
    }
    enum Operation {
        case increment, decrement
    }
    mutating func set(_ component: DateComponent, _ operation: Operation, by value: Int) {
        if component == .month {
            if operation == .increment {
                guard let _ = self.month else { return }
                self.month = self.month! + 1
            }
            else if operation == .decrement {
                guard let _ = self.month else { return }
                self.month = self.month! - 1
            }
        }
        else if component == .year {
            if operation == .increment {
                guard let _ = self.year else { return }
                self.year = self.year! + 1
            }
            else if operation == .decrement {
                guard let _ = self.year else { return }
                self.year = self.year! - 1
            }
        }
    }
    
    mutating func set(_ component: DateComponent, by value: Int) {
        if component == .month {
            self.month = value
        }
        else if component == .year {
            self.year = value
        }
    }
}
