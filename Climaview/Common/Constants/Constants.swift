//
//  Constants.swift
//  Climaview
//
//  Created by Akshay Phadke on 11/12/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation

let countries = ["UK","England","Scotland","Wales"]

enum UserDefaultsKey: String {
    case isSelected
    
    static func countryWeatherData(for country: String) -> String {
        let key = country + "WeatherData"
        return key
    }
    
}
