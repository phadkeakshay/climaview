//
//  WeatherDataViewController.swift
//  Climaview
//
//  Created by Akshay Phadke on 11/12/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class WeatherDataViewController: UIViewController {
    
    private var weatherData: WeatherData!
    private var country: Country!
    var countriesTableViewCellRowHeight: CGFloat = 50.0
    var currentlyDisplayedDateComponent: DateComponents!
    
    @IBOutlet weak var countriesTableView: UITableView!
    @IBOutlet weak var yearMonthStackView: UIStackView!
    @IBOutlet weak var weatherDataStackView: UIStackView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var rainfallValueLabel: UILabel!
    @IBOutlet weak var minimumTempratureValueLabel: UILabel!
    @IBOutlet weak var maximumTempratureValueLabel: UILabel!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var yearMonthButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfiguration()
        if let country = UserDefaults.standard.value(forKey: UserDefaultsKey.isSelected.rawValue) as? String {
            if let data = UserDefaults.standard.value(forKey: UserDefaultsKey.countryWeatherData(for: country)) as? Data {
                load(data: data, for: country)
            }
        }
        configureTableView()
    }
    
    //MARK:- Helper Methods
    fileprivate func initialConfiguration() {
        
    }
    
    fileprivate func configureTableView() {
        countriesTableView.rowHeight = UITableView.automaticDimension
        countriesTableView.estimatedRowHeight = 50.0
    }
    
    fileprivate func load(data: Data, for countryName: String, isBySelectingCountry: Bool = false) {
        do {
            country = try PropertyListDecoder().decode(Country.self, from: data)
            weatherData = WeatherData()
            weatherData.rainfall = country.weatherData.rainfall.sorted(by: { (metric1, metric2) -> Bool in
                return (metric1.ID < metric2.ID)
            })
            weatherData.minimumTempratures = country.weatherData.minimumTempratures.sorted(by: { (metric1, metric2) -> Bool in
                return (metric1.ID < metric2.ID)
            })
            weatherData.maximumTempratures = country.weatherData.maximumTempratures.sorted(by: { (metric1, metric2) -> Bool in
                return (metric1.ID < metric2.ID)
            })
            currentlyDisplayedDateComponent = country.dateComponents.last
            if isBySelectingCountry {
                UserDefaults.standard.setValue(countryName, forKey: UserDefaultsKey.isSelected.rawValue)
                UserDefaults.standard.synchronize()
            }
            DispatchQueue.main.async {
                self.countryNameLabel.isHidden = false
                self.yearMonthStackView.isHidden = false
                self.yearMonthButton.isEnabled = true
                self.messageView.isHidden = true
                self.view.sendSubviewToBack(self.messageView)
                self.weatherDataStackView.isHidden = false
                self.countriesTableView.reloadData()
                self.set(countryNameLabelWith: self.country.name)
                self.set(monthYearLabelWith: self.currentlyDisplayedDateComponent.month!, with: self.currentlyDisplayedDateComponent.year!)
                self.set(weatherDataWith: self.country.weatherData, at: self.country.dateComponents.index(of: self.currentlyDisplayedDateComponent) ?? 0)
                let index = self.country.dateComponents.index(of: self.currentlyDisplayedDateComponent) ?? 0
                if index == (self.country.dateComponents.count - 1) {
                    self.nextButton.isEnabled = false
                }
            }
        }
        catch {
            Utils.presentAlertController(withTitle: "Error", withMessage: error.localizedDescription, okCompletionHandler: {
                
            })
        }
    }
    
    fileprivate func set(countryNameLabelWith countryName: String) {
        countryNameLabel.text = countryName
    }
    
    fileprivate func set(monthYearLabelWith month: Int, with year: Int) {
        let yearMonthString = "\(Utils.monthName(for: month)) \(year)"
        //monthYearLabel.text = text
        let yearMonthAttributedString = NSAttributedString(string: yearMonthString, attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 24.0) as Any])
        let tapToSelectAttributedString = NSAttributedString(string: "\r(Tap To Select)", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 14.0) as Any])
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(yearMonthAttributedString)
        mutableAttributedString.append(tapToSelectAttributedString)
        monthYearLabel.attributedText = mutableAttributedString
        
    }
    
    fileprivate func set(weatherDataWith weatherData: WeatherData, at index: Int) {
        let rainfallValueString = String(format: "%.2f", weatherData.rainfall[index].value)// + "\r(mm)"
        //rainfallValueLabel.text = rainfallValue
        let rainfallValueAttributedString = NSAttributedString(string: rainfallValueString, attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 34.0) as Any])
        let rainfallUnitAttributedString = NSAttributedString(string: "\r(mm)", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 22.0) as Any])
        let rainfallMutableAttributedString = NSMutableAttributedString()
        rainfallMutableAttributedString.append(rainfallValueAttributedString)
        rainfallMutableAttributedString.append(rainfallUnitAttributedString)
        rainfallValueLabel.attributedText = rainfallMutableAttributedString
        
        
        let minimumTempratureValueString = String(format: "%.2f", weatherData.minimumTempratures[index].value)// + "\r(°C)"
        //minimumTempratureValueLabel.text = minimumTempratureValue
        let minimumTempratureValueAttributedString = NSAttributedString(string: minimumTempratureValueString, attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 34.0) as Any])
        let minimumTempratureUnitAttributedString = NSAttributedString(string: "\r(°C)", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 22.0) as Any])
        let minimumTempratureMutableAttributedString = NSMutableAttributedString()
        minimumTempratureMutableAttributedString.append(minimumTempratureValueAttributedString)
        minimumTempratureMutableAttributedString.append(minimumTempratureUnitAttributedString)
        minimumTempratureValueLabel.attributedText = minimumTempratureMutableAttributedString
        
        let maximumTempratureValueString = String(format: "%.2f", weatherData.maximumTempratures[index].value)// + "\r(°C)"
        //maximumTempratureValueLabel.text = maximumTempratureValue
        let maximumTempratureValueAttributedString = NSAttributedString(string: maximumTempratureValueString, attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 34.0) as Any])
        let maximumTempratureUnitAttributedString = NSAttributedString(string: "\r(°C)", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 22.0) as Any])
        let maximumTempratureMutableAttributedString = NSMutableAttributedString()
        maximumTempratureMutableAttributedString.append(maximumTempratureValueAttributedString)
        maximumTempratureMutableAttributedString.append(maximumTempratureUnitAttributedString)
        maximumTempratureValueLabel.attributedText = maximumTempratureMutableAttributedString
    }
    
    //MARK:- Button Actions
    @IBAction func actionNextMonth(_ sender: UIButton, forEvent event: UIEvent) {
        if currentlyDisplayedDateComponent.month! == 12 {
            currentlyDisplayedDateComponent.set(.month, by: 1)
            currentlyDisplayedDateComponent.set(.year, .increment, by: 1)
        }
        else {
            currentlyDisplayedDateComponent.set(.month, .increment, by: 1)
            let index = country.dateComponents.index(of: currentlyDisplayedDateComponent) ?? 0
            if index == (country.dateComponents.count - 1) {
                nextButton.isEnabled = false
            }
        }
        if !previousButton.isEnabled {
            previousButton.isEnabled = true
        }
        set(monthYearLabelWith: currentlyDisplayedDateComponent.month!, with: currentlyDisplayedDateComponent.year!)
        set(weatherDataWith: country.weatherData, at: country.dateComponents.index(of: currentlyDisplayedDateComponent) ?? 0)
    }
    
    @IBAction func actionPreviousMonth(_ sender: UIButton, forEvent event: UIEvent) {
        if currentlyDisplayedDateComponent.month! == 1 {
            currentlyDisplayedDateComponent.set(.month, by: 12)
            currentlyDisplayedDateComponent.set(.year, .decrement, by: 1)
        }
        else {
            currentlyDisplayedDateComponent.set(.month, .decrement, by: 1)
            let index = country.dateComponents.index(of: currentlyDisplayedDateComponent) ?? 0
            if index == 0 {
                previousButton.isEnabled = false
            }
        }
        if !nextButton.isEnabled {
            nextButton.isEnabled = true
        }
        set(monthYearLabelWith: currentlyDisplayedDateComponent.month!, with: currentlyDisplayedDateComponent.year!)
        set(weatherDataWith: country.weatherData, at: country.dateComponents.index(of: currentlyDisplayedDateComponent) ?? 0)
    }
    
    @IBAction func actionYearMonthSelectioon(_ sender: UIButton, forEvent event: UIEvent) {
        var months = [String](), years = [String]()
        for index in 1...12 {
            months.append(Utils.monthName(for: index))
        }
        for index in 0..<country.dateComponents.count {
            let dateComponent = country.dateComponents[index]
            guard let year = dateComponent.year else { return }
            if !years.contains(String(year)) {
                years.append(String(year))
            }
        }
        
        ActionSheetMultipleStringPicker.show(withTitle: "Select Month And/Or Year", rows: [months, years], initialSelection: [currentlyDisplayedDateComponent.month!-1,years.index(of: String(currentlyDisplayedDateComponent.year!)) ?? 0], doneBlock: { (picker, indexes, values) in
            guard let values = values as? NSArray else { return }
            if (self.currentlyDisplayedDateComponent.month! != Utils.monthIndex(for: (values[0] as! String))) || (self.currentlyDisplayedDateComponent.year! != Int(values[1] as! String)) {
                self.currentlyDisplayedDateComponent.month = Utils.monthIndex(for: (values[0] as! String))
                self.currentlyDisplayedDateComponent.year = Int(values[1] as! String)
                self.set(monthYearLabelWith: self.currentlyDisplayedDateComponent.month!, with: self.currentlyDisplayedDateComponent.year!)
                self.set(weatherDataWith: self.country.weatherData, at: self.country.dateComponents.index(of: self.currentlyDisplayedDateComponent) ?? 0)
                let index = self.country.dateComponents.index(of: self.currentlyDisplayedDateComponent) ?? 0
                if index == (self.country.dateComponents.count - 1) {
                    self.nextButton.isEnabled = false
                    self.previousButton.isEnabled = true
                }
                else if index == 0 {
                    self.nextButton.isEnabled = false
                    self.previousButton.isEnabled = true
                }
                else {
                    self.nextButton.isEnabled = true
                    self.previousButton.isEnabled = true
                }
            }
            else {
                //
            }
        }, cancel: { (picker) in
            
        }, origin: sender)
    }
    
    
    //MARK:- API Calls
    fileprivate func get(metric: Metric.MetricType? = nil, for country: String) {
        if metric == nil {
            Utils.showProgressHUD(for: self.view)
            get(metric: .rainfall, for: country)
            return
        }
        else {

        }
        
        var metricParameterName = ""
        switch metric! {
            case .rainfall:
                metricParameterName = Metric.MetricParameterName.rainfall.rawValue
            case .minimumTemprature:
                metricParameterName = Metric.MetricParameterName.minimumTemprature.rawValue
            case .maximumTemprature:
                metricParameterName = Metric.MetricParameterName.maximumTemprature.rawValue
        }
        let path = "\(metricParameterName)-\(country).json"
        RestClient.request(atPath: path, forHTTPMethod: .get, withParams: nil, byShowingProgressHUD: false, forView: nil) { (error, response) in
            if error == nil {
                if let json = response as? [[String: Any]] {
                    var metrics = [Metric]()
                    for index in 0..<json.count {
                        metrics.append(Metric(json: json[index], metricType: .rainfall, ID: (index+1)))
                    }
                    if !metrics.isEmpty {
                        if self.weatherData == nil {
                            self.weatherData = WeatherData()
                        }
                        switch metric! {
                        case .rainfall:
                            self.weatherData.rainfall = metrics.sorted(by: { (metric1, metric2) -> Bool in
                                return (metric1.ID < metric2.ID)
                            })
                        case .minimumTemprature:
                            self.weatherData.minimumTempratures = metrics.sorted(by: { (metric1, metric2) -> Bool in
                                return (metric1.ID < metric2.ID)
                            })
                        case .maximumTemprature:
                            self.weatherData.maximumTempratures = metrics.sorted(by: { (metric1, metric2) -> Bool in
                                return (metric1.ID < metric2.ID)
                            })
                        }
                        
                    }
                }
                
                switch metric! {
                    case .rainfall:
                        self.get(metric: .minimumTemprature, for: country)
                    case .minimumTemprature:
                        self.get(metric: .maximumTemprature, for: country)
                    case .maximumTemprature:
                        Utils.hideProgressHUD(for: self.view)
                        let countryData = Country(name: country, weatherData: self.weatherData)
                        self.country = countryData
                        self.currentlyDisplayedDateComponent = self.country.dateComponents.last
                        do {
                            let data = try PropertyListEncoder().encode(countryData)
                            UserDefaults.standard.set(data, forKey: UserDefaultsKey.countryWeatherData(for: country))
                            UserDefaults.standard.setValue(country, forKey: UserDefaultsKey.isSelected.rawValue)
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async {
                                self.countryNameLabel.isHidden = false
                                self.yearMonthStackView.isHidden = false
                                self.yearMonthButton.isEnabled = true
                                self.messageView.isHidden = true
                                self.view.sendSubviewToBack(self.messageView)
                                self.weatherDataStackView.isHidden = false
                                self.countriesTableView.reloadData()
                                self.set(countryNameLabelWith: self.country.name)
                                self.set(monthYearLabelWith: self.currentlyDisplayedDateComponent.month!, with: self.currentlyDisplayedDateComponent.year!)
                                self.set(weatherDataWith: self.country.weatherData, at: self.country.dateComponents.index(of: self.currentlyDisplayedDateComponent) ?? 0)
                                let index = self.country.dateComponents.index(of: self.currentlyDisplayedDateComponent) ?? 0
                                if index == (self.country.dateComponents.count - 1) {
                                    self.nextButton.isEnabled = false
                                }
                            }
                        }
                        catch {
                            Utils.presentAlertController(withTitle: "Error", withMessage: error.localizedDescription, okCompletionHandler: {
                                
                            })
                        }
                }
                
            }
            else {
                Utils.hideProgressHUD(for: self.view)
                Utils.presentAlertController(withTitle: "Error", withMessage: "Something error occured. Please try again.", okCompletionHandler: {
                    
                })
            }
        }
    }
}

//MARK:- UITableViewDataSource
extension WeatherDataViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.cellIdentifier, for: indexPath) as? CountryTableViewCell else { return UITableViewCell() }
        cell.configure(with: countries[indexPath.row])
        return cell
    }
}


//MARK:- UITableViewDelegate
extension WeatherDataViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let data = UserDefaults.standard.value(forKey: UserDefaultsKey.countryWeatherData(for: countries[indexPath.row])) as? Data {
            if let selectedCountryName = UserDefaults.standard.value(forKey: UserDefaultsKey.isSelected.rawValue) as? String {
                if selectedCountryName != countries[indexPath.row] {
                    load(data: data, for: countries[indexPath.row], isBySelectingCountry: true)
                }
            }
        }
        else {
            get(for: countries[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return countriesTableViewCellRowHeight
    }
}
